# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit git-r3

DESCRIPTION="A git utility for checking out and tracking a mercurial repo."
HOMEPAGE="https://github.com/cosmin/git-hg"
SRC_URI=""
EGIT_REPO_URI="https://github.com/cosmin/git-hg.git"
EGIT_COMMIT="e8fcbc9d8d2634fb79867aa0520bcde5e21468bb"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

IUSE=""
DEPEND=""
RDEPEND="${DEPEND}"
