# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

PYTHON_COMPAT=(python2_7)

inherit distutils-r1

DESCRIPTION="python implementation of axolotl-curve25519 encryption"
HOMEPAGE="https://github.com/tgalal/python-axolotl-curve25519"
SRC_URI="mirror://pypi/p/python-axolotl-curve25519/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="sys-libs/zlib"
RDEPEND="${DEPEND}"
